## What we want you to do

* Take a look at the code and try to refactor it as good as you can. Point out everything that seem odd to you like names,
   empty lines literally whatever.
  <br/><br/>
* You are provided with the class `CurrencyWallet`. Inside this class you can only find a `BigDecimal` `plnBudget`. We would
   like you to implement a separate controller that will allow you to withdraw some money from the wallet pretending
   like you are buying currencies. The set of currencies you should buy are defined by the response of the already existing
   piece of code in the `NbpRestClient` specifically the `getCurrencyTable(...)` method. You should buy exactly one
   unit for each currency from the table `A`.
   <br/><br/>
   The response of `getCurrencyTable(...)` response looks as follows:

   ```json
   [
      {
         "table": "A",
         "no": "056/A/NBP/2021",
         "effectiveDate": "2021-03-23",
         "rates": [
            {
               "currency": "bat",
               "code": "THB",
               "price": 0.1251
            },
            {
               "currency": "american dollar",
               "code": "USD",
               "price": 4.7946
            },
            {
               "currency": "euro",
               "code": "EUR",
               "price": 4.7846
            },
            {
               "currency": "brazilian real",
               "code": "BRL",
               "price": 3.8746
            }
         ]
      }
   ]
   ```

* Check if you have enough money in your budget to buy all the currencies. If not, try to show it somehow. 
<br/><br/>
* Implement endpoint `/balance` which displays how much money you have left in the budget and what currencies you
have bought.