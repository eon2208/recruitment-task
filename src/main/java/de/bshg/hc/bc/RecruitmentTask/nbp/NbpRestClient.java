package de.bshg.hc.bc.RecruitmentTask.nbp;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static de.bshg.hc.bc.RecruitmentTask.nbp.NbpConstants.*;

@FeignClient(name = "NBP", url = NBP_URL)
public interface NbpRestClient {

    @GetMapping(CURRENCY_TABLES + "/{tableType}")
    ResponseEntity<String> getCurrencyTable(
            @PathVariable String tableType
    );

    @GetMapping(RATES + "/{tableType}/{currencyCode}")
    ResponseEntity<String> getCurrencyRate(
            @PathVariable String tableType,
            @PathVariable String currencyCode
    );

}
